package jraven.calculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainWindow extends AppCompatActivity implements OnClickListener{

    private TextView measurementTextView;
    private int counter;
    private int measurement;
    private int overallMeasurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_window);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        measurementTextView = (TextView) findViewById(R.id.measurementTextView);
        counter = 0;
        List<Button> buttons;
        int[] BUTTON_IDS = {
                R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5,
                R.id.button6, R.id.button7, R.id.button8, R.id.button9, R.id.button0,
                R.id.buttonClear, R.id.buttonDone,};

        buttons = new ArrayList<>();

        for (int id : BUTTON_IDS) {
            Button button = (Button)findViewById(id);
            button.setOnClickListener(this);
            buttons.add(button);
        }

    }
    public void onClick (View v) {

        if (measurementTextView.getText().toString().equals("Enter Measurement") || measurementTextView.getText().toString().equals("Enter the overall length")) {
            measurementTextView.setText("");
        }
        switch(v.getId()) {

            case R.id.button1:
                measurementTextView.append("1");
                break;

            case R.id.button2:
                measurementTextView.append("2");
                break;

            case R.id.button3:
                measurementTextView.append("3");
                break;

            case R.id.button4:
                measurementTextView.append("4");
                break;

            case R.id.button5:
                measurementTextView.append("5");
                break;

            case R.id.button6:
                measurementTextView.append("6");
                break;

            case R.id.button7:
                measurementTextView.append("7");
                break;

            case R.id.button8:
                measurementTextView.append("8");
                break;

            case R.id.button9:
                measurementTextView.append("9");
                break;

            case R.id.button0:
                measurementTextView.append("0");
                break;

            case R.id.buttonDone:
                counter++;

                try{
                    if (counter == 1) {
                        measurement = Integer.parseInt(measurementTextView.getText().toString());
                        measurementTextView.setText("Enter the overall length");
                    }
                    else
                        if (counter == 2 ) {
                            overallMeasurement = Integer.parseInt(measurementTextView.getText().toString());
                            CalculateMeasurement(measurement, overallMeasurement);
                            counter = 0;
                            measurementTextView.setText("Enter Measurement");
                            measurement = 0;
                            overallMeasurement = 0;
                        }

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Number hasn't Been Entered", Toast.LENGTH_SHORT).show();
                    counter = 0;
                }
                break;

            case R.id.buttonClear:
                measurementTextView.setText("");
                break;
        }
    }

    public void CalculateMeasurement(int num, int overallNum) {
        Intent intent = new Intent( this, DisplayCalculatedNumbers.class);
        int num1 = num;
        int num2 = num;

        ArrayList<Integer> array = new ArrayList<>();

        do {
            num2 = calculateFactorial(num1, num2);
            array.add(num2);
        }
        while (num2 < overallNum);

        intent.putExtra("Array List", array);
        startActivity(intent);

    }

    private int calculateFactorial(int num1, int num2) {
        return num1 + num2;
    }

    protected void onStop() {
        super.onStop();

        counter = 0;
        measurementTextView.setText("Enter Measurement");
        measurement = 0;
        overallMeasurement = 0;
    }
}
