package jraven.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class DisplayCalculatedNumbers extends AppCompatActivity {
    private ArrayList<Integer> retrievedArray;
    private RecyclerView mRecyclerView;
    private MyListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_calculated_numbers);
        retrievedArray = (ArrayList<Integer>) getIntent().getSerializableExtra("Array List");

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.measurementsListView);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new MyListAdapter(retrievedArray);
        mRecyclerView.setAdapter(mAdapter);
    }

    protected void onStop() {
        super.onStop();

        retrievedArray.clear();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {
        private ArrayList<Integer> mDataSet;

        public MyListAdapter(ArrayList<Integer> myDataSet) {
            this.mDataSet = myDataSet;
        }

        @Override
        public MyListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_measurements, parent, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyListAdapter.ViewHolder holder, int position) {
            final Integer measurement = mDataSet.get(position);
            holder.measurement.setText(measurement.toString());

        }

        @Override
        public int getItemCount() {
            return mDataSet.size();
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private View layout;
            private TextView measurement;

            public ViewHolder(View itemView) {
                super(itemView);

                layout = itemView;
                measurement = (TextView) itemView.findViewById(R.id.measurement);
            }
        }

    }

}
