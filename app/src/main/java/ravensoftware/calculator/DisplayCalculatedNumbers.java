package ravensoftware.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DisplayCalculatedNumbers extends AppCompatActivity {
    public View view;
    public TextView textBox2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_calculated_numbers);

        Intent intent = getIntent();

        ArrayList<Double> retrievedArray = (ArrayList<Double>) getIntent().getSerializableExtra("Array List");

        String formattedStringOfDoubles = retrievedArray.toString()
        .replace("[", "")
        .replace("]", "")
        .trim();

        textBox2 = (TextView) findViewById(R.id.textView2);
        textBox2.setMovementMethod(new ScrollingMovementMethod());
        textBox2.setText(formattedStringOfDoubles);
    }
}
