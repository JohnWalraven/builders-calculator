package jraven.calculator;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainWindowTest {

    @Rule
    public ActivityTestRule<MainWindow> mActivityTestRule = new ActivityTestRule<>(MainWindow.class);

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    @Test
    public void mainWindowTest() {

//        ViewInteraction button = onView(
//                allOf(withId(R.id.button1),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//
//                                1),
//                        isDisplayed()));
//        button.check(matches(isDisplayed()));
//
//        ViewInteraction button2 = onView(
//                allOf(withId(R.id.button2),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//                                2),
//                        isDisplayed()));
//        button2.check(matches(isDisplayed()));
//
//        ViewInteraction button3 = onView(
//                allOf(withId(R.id.button3),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//
//                                3),
//                        isDisplayed()));
//        button3.check(matches(isDisplayed()));
//
//        ViewInteraction button4 = onView(
//                allOf(withId(R.id.button4),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//
//                                4),
//                        isDisplayed()));
//        button4.check(matches(isDisplayed()));
//
//        ViewInteraction button5 = onView(
//                allOf(withId(R.id.button5),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//
//                                5),
//                        isDisplayed()));
//        button5.check(matches(isDisplayed()));
//
//        ViewInteraction button6 = onView(
//                allOf(withId(R.id.button6),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//
//                                6),
//                        isDisplayed()));
//        button6.check(matches(isDisplayed()));
//
//        ViewInteraction button7 = onView(
//                allOf(withId(R.id.button7),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//
//                                7),
//                        isDisplayed()));
//        button7.check(matches(isDisplayed()));
//
//        ViewInteraction button8 = onView(
//                allOf(withId(R.id.button8),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//
//                                8),
//                        isDisplayed()));
//        button8.check(matches(isDisplayed()));
//
//        ViewInteraction button9 = onView(
//                allOf(withId(R.id.button9),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//                                9),
//                        isDisplayed()));
//        button9.check(matches(isDisplayed()));
//
//        ViewInteraction button10 = onView(
//                allOf(withId(R.id.buttonClear),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//
//                                10),
//                        isDisplayed()));
//        button10.check(matches(isDisplayed()));
//
//        ViewInteraction button11 = onView(
//                allOf(withId(R.id.button0),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//                                11),
//                        isDisplayed()));
//        button11.check(matches(isDisplayed()));
//
//        ViewInteraction button12 = onView(
//                allOf(withId(R.id.buttonDone),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//                                12),
//                        isDisplayed()));
//        button12.check(matches(isDisplayed()));
//
//        ViewInteraction button13 = onView(
//                allOf(withId(R.id.buttonDone),
//                        childAtPosition(
//                                        IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
//                                12),
//                        isDisplayed()));
//        button13.check(matches(isDisplayed()));

        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.measurementTextView), withText("Enter Measurement"), isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.button2), withText("2"), isDisplayed()));
        appCompatButton.perform(click());


        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.button5), withText("5"), isDisplayed()));
        appCompatButton2.perform(click());


        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.button0), withText("0"), isDisplayed()));
        appCompatButton3.perform(click());


        ViewInteraction appCompatButton4 = onView(
                allOf(withId(R.id.buttonDone), withText("Done"), isDisplayed()));
        appCompatButton4.perform(click());

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.button5), withText("5"), isDisplayed()));
        appCompatButton5.perform(click());


        ViewInteraction appCompatButton6 = onView(
                allOf(withId(R.id.button0), withText("0"), isDisplayed()));
        appCompatButton6.perform(click());

        ViewInteraction appCompatButton7 = onView(
                allOf(withId(R.id.button0), withText("0"), isDisplayed()));
        appCompatButton7.perform(click());

        ViewInteraction appCompatButton8 = onView(
                allOf(withId(R.id.button0), withText("0"), isDisplayed()));
        appCompatButton8.perform(click());

        ViewInteraction appCompatButton9 = onView(
                allOf(withId(R.id.buttonDone), withText("Done"), isDisplayed()));
        appCompatButton9.perform(click());

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.measurement), withText("500"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.measurementsListView),
                                        0),
                                1),
                        isDisplayed()));
        textView2.check(matches(isDisplayed()));

        ViewInteraction checkBox = onView(
                allOf(withId(R.id.checkBox),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.measurementsListView),
                                        0),
                                0),
                        isDisplayed()));
        checkBox.check(matches(isDisplayed()));

        ViewInteraction relativeLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.measurementsListView),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                        0)),
                        0),
                        isDisplayed()));
        relativeLayout.check(matches(isDisplayed()));

    }
}
