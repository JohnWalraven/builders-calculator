This is a little Android application I built for my brother, who is a builder. 
It was designed for the purpose of providing quick measurements when he was working on a roof, it does this by taking an initial measurement, which is the spacing required, and then takes the overall measurement you need. The app then multiplies the initial measurement by itself until it reaches the overall measurement required.  

The app then serializes and passes an arraylist of measurements(which are integers) to the next activity through an Intent. Once the next activity has received the arraylist, it sends it to listview to be displayed.

This is the first application I built and what got me interested in Android development, it was intended to be incredibly basic and my brother does use it when he needs to.